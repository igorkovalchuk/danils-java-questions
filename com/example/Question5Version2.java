package com.example;

public class Question5Version2 {

    public static void main(String[] args) {
        //test();

        int[] numbers = new int[]{1, 2, 3};
        double[] weights = new double[]{1, 2, 10};
        System.out.println("Result = " + getNumber(numbers, weights));
    }

    // В предыдущем решении, если weights = new int[10000,200,300000]
    // нам нужно было бы создавать очень большой дополнительный массив.
    // Кроме того мы не могли использовать дробные значения weight.
    //
    // Но, используя предыдущее простое решение теперь мы можем написать 
    // алгоритм получше.
    //
    // С каждой ячейкой числа мы можем связать как бы диапазон ее весовых значений;
    // это может быть не совсем правильное название, но я затрудняюсь дать название точнее.
    // Это как бы "составной" отрезок, который включает в себя диапазоны веса каждого числа.
    //
    // Если у нас есть
    // numbers: {1, 2, 3}
    // weights: {1, 2, 10}
    // то диапазоны получаем так:
    //
    // Для 1 это будет вес = от 0 до 1
    // для 2 вес= от 1 до 3
    // и для 3 вес = от 3 до 13.
    //
    // Генерируя случайное число мы будем получать 
    // число для этого диапазона от 0 до 13,
    // потом зная к какому диапазону относится число, мы легко выберем число.
    //
    // Мы рассчитываем на то что ф-ия Math.random генерирует
    // случайное число с примерно одинаковой вероятностью.
    // Поэтому получить случайное число из диапазона весов для числа 3 будет
    // в 10 раз легче чем для числа 1.
    private static int getNumber(int[] numbers, double[] weights) {

        double[] summaryWeightsStart = new double[weights.length];
        double[] summaryWeightsEnd = new double[weights.length];

        double sum = 0;
        for (int i = 0; i < weights.length; i++) {
            summaryWeightsStart[i] = sum;
            sum = sum + weights[i];
            summaryWeightsEnd[i] = sum;
        }

        // random обязательно будет лежать в пределах от 0 до 13, не включая 13.0
        double random = (Math.random() * sum);

        int result = -1;
        
        for(int i = 0; i < weights.length; i++) {
            if (random >= summaryWeightsStart[i] && random <= summaryWeightsEnd[i]) {
                result = numbers[i];
                break;
            }
        }

        // если в нашей программе была бы ошибка, 
        // то в результате мы получили бы -1
        if (result == -1) {
            throw new RuntimeException("Наш алгоритм работает неправильно, результат не найден.");
        }

        return result;
    }

    private static void test() {
        // Простой тест, на всякий случай
        System.out.println("Start the test");
        int[] numbers = new int[]{1, 2, 3};
        for(int i = 1; i < 10000000; i++) {
            double[] weights = new double[]{
                1,
                1 + Math.random() * 10,
                1 + Math.random() * 100
            };
            getNumber(numbers, weights);
        }
        System.out.println("Test completed");
    }
}
