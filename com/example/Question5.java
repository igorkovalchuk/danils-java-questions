package com.example;

public class Question5 {

    public static void main(String[] args) {
        int[] numbers = new int[]{1, 2, 3};
        int[] weights = new int[]{1, 2, 10};
        System.out.println("Result = " + getNumber(numbers, weights));
    }

    private static int getNumber(int[] numbers, int[] weights) {
        // Здесь проще всего сгенерировать еще один - дополнительный массив
        // 1, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3
        // и уже по нему выполнить случайный выбор.

        int len = 0;
        for (int w : weights) {
            len = len + w;
        }

        int[] tmp = new int[len];
        int tmpIndex = 0;

        for(int i = 0; i < numbers.length; i++) {
            int n = numbers[i];
            int w = weights[i];
            for(int j = 1; j <= w; j++) {
                tmp[tmpIndex] = n;
                tmpIndex++;
            }
        }

        int resultIndex = (int)(Math.random() * len);
        return tmp[resultIndex];
    }
}
