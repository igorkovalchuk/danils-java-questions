package com.example;

public class Question7Version2 {

    // точность поиска
    private static final double PRECISION = 0.001;

    private static int iterations = 0;

    /**
     * Берем отрезок от 0 до 10 и делим пополам
     * получаем отреок от 0 до 5 и от 5 до 10
     * после этого случайным образом выбираем один из них
     * и повторяем это множество раз.
     * 
     * Этот алгоритм будет работать для любой ф-ии,
     * которая пересекает ось X,
     * но вычислений будет много.
     * 
     * Иногда меньше, чем если перебирать все тысячные доли отрезка последовательно.
     * Но иногда - в несколько раз больше.
     * 
     * Если ф-ия не пересекает ось X, а только касается ее,
     * как парабола с одним корнем,
     * этот алгоритм, как правило, не сможет найти такую точку.
     *
     * В реальной жизни я бы не использовал такой алгоритм,
     * слишком уж он ненадежный.
     * Я бы поискал что-то получше.
     */
    public static void main(String[] args) {
        double x1 = 0;
        double x2 = 10;

        MyFunction f = new MyFunction();

        int serialIterations = (int) ((x2 - x1) / PRECISION);
        System.out.println("При последовательном поиске, " +
                "максимальное число попыток поиска: " + serialIterations);

        Double result = find(f, x1, x2, 0);

        System.out.println();
        System.out.println("Попыток поиска при делении отрезков " +
                "и случайном выборе: " + iterations);

        if (result == null) {
            System.out.println("Функция не пересекает ось X");
        } else {
            System.out.println("X = " + result);
        }
    }

    private static Double find(MyFunction f, double x1, double x2, int depth) {
        depth++;

        // Получаем 2 отрезка:
        // x1 .. m
        // m .. x2
        double m = x1 + (x2 - x1) / 2;

        Double result;

        double rand = Math.random();
        if (rand < 0.5) {
            result = analyze(f, x1, m, depth);
            if (result == null) {
                result = analyze(f, m, x2, depth);
            }
        }
        else {
            result = analyze(f, m, x2, depth);
            if (result == null) {
                result = analyze(f, x1, m, depth);
            }
        }

        return result;
    }

    private static Double analyze(MyFunction f, double x1, double x2, int depth) {
        iterations++;

        //System.out.println("  ".repeat(depth) + "Рассматриваем отрезок: " + x1 + " .. " + x2);

        double y1 = f.getY(x1);
        double y2 = f.getY(x2);

        // Этот отрезок должен или пересекать Y = 0 или заканчиваться в Y = 0.
        // при этом x2 - x1 < 0.001
        // тогда мы можем сказать что мы нашли корень (т.е. X).

        double dx = x2 - x1;

        if (y1 == 0) {
            // корень = x1
            return x1;
        }
        else if (y2 == 0) {
            // корень = x2
            return x2;
        }
        else if (y1 < 0 && y2 >0) {
            // y1 находится под осью x
            // y2 находится над осью x
            if (dx > PRECISION) {
                // ищем дальше (т.е. точнее) ... это уже рекурсивный вызов, т.е. вглубь
                return find(f, x1, x2, depth);
            }
            else {
                // можем брать x1 или x2 как результат
                return x1;
            }
        }
        else if (y2 < 0 && y1 > 0) {
            // y1 находится над осью x
            // y2 находится под осью x
            if (dx > PRECISION) {
                // ищем дальше (т.е. точнее) ... это уже рекурсивный вызов, т.е. вглубь
                return find(f, x1, x2, depth);
            }
            else {
                // можем брать x1 или x2 как результат
                return x1;
            }
        }
        // обе точки находятся над осью x,
        // или под осью x
        else if (dx > PRECISION) {             
            // продолжаем делить отрезок
            return find(f, x1, x2, depth);
        }
        else {
            // конечно, может быть ситуация, когда мы не сможем найти корень,
            // т.к. заданная точность поиска может не позволить нам это сделать
            // если ф-ия резко изменяет свое значение на очень маленьком участке.
            // но в большинстве случаев это вполне нормально считать что здесь нет корня.
            return null;
        }

    }

    static class MyFunction {

        public double getY(double x) {
            // эта парабола пересекает ось X и имеет два корня, x=2 и x=6
            return (x - 4) * (x - 4) - 4;
        }

    }
}
